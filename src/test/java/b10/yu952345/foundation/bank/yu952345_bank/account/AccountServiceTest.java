package b10.yu952345.foundation.bank.yu952345_bank.account;

import b10.yu952345.foundation.bank.yu952345_bank.customer.Customer;
import b10.yu952345.foundation.bank.yu952345_bank.customer.CustomerRepository;
import b10.yu952345.foundation.bank.yu952345_bank.exception.BusinessException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class AccountServiceTest {

    @InjectMocks
    private AccountService accountService;

    @Mock
    private AccountRepository accountRepositoryMock;

    @Mock
    private CustomerRepository customerRepositoryMock;

    @Test
    public void shouldThrowBusinessExceptionWhenCustomerNotFound() {
        Integer customerId = 1;

        given(customerRepositoryMock.findById(eq(customerId))).willReturn(Optional.empty());

        BusinessException exception = assertThrows(BusinessException.class, () -> accountService.create(customerId, new Account()));
        assertEquals(String.format("Customer with id %d not found", customerId), exception.getMessage());
    }

    @Test
    public void shouldCreateAccountSuccessfully() throws Exception {
        Integer customerId = 1;
        Customer customer = new Customer();
        customer.setId(customerId);

        Account account = new Account();
        account.setType(AccountType.SAVINGS);

        given(customerRepositoryMock.findById(eq(customerId))).willReturn(Optional.of(customer));
        given(accountRepositoryMock.save(eq(account))).willReturn(account);

        account = accountService.create(customerId, account);

        verify(accountRepositoryMock, times(1)).save(any(Account.class));
        verify(customerRepositoryMock, times(1)).save(any(Customer.class));

        assertEquals(0.0, account.getBalance(), 0.01);
        assertEquals(customerId, account.getCustomerId());
        assertEquals(customer, account.getCustomer());
    }

    @Test
    public void shouldReturnNullWhenAccountNotFound() {
        Integer customerId = 1;
        Integer accountId = 2;

        given(accountRepositoryMock.findByCustomerIdAndId(eq(customerId), eq(accountId))).willReturn(Optional.empty());

        assertNull(accountService.findById(customerId, accountId));
    }

    @Test
    public void shouldReturnAnAccountWhenAccountFound() {
        Integer customerId = 1;
        Integer accountId = 2;
        Account expectedAccount = new Account();

        given(accountRepositoryMock.findByCustomerIdAndId(eq(customerId), eq(accountId))).willReturn(Optional.of(expectedAccount));

        Account account = accountService.findById(customerId, accountId);

        assertEquals(expectedAccount, account);
    }

    @Test
    public void shouldThrowExceptionWhenAccountNotFound() {
        Integer customerId = 1;
        Integer accountId = 2;

        given(accountRepositoryMock.findByCustomerIdAndId(eq(customerId), eq(accountId))).willReturn(Optional.empty());

        Exception exception = assertThrows(BusinessException.class, () -> accountService.deposit(customerId, accountId, 1000.0));
        assertEquals(String.format("Account not found for customer id %d and account id %d", customerId, accountId), exception.getMessage());
    }

    @Test
    public void shouldThrowExceptionWhenDepositValueIsNegative() {
        Integer customerId = 1;
        Integer accountId = 2;

        Exception exception = assertThrows(BusinessException.class, () -> accountService.deposit(customerId, accountId, -1.0));
        assertEquals("Deposit value must be greater than 0", exception.getMessage());
    }

    @Test
    public void shouldThrowExceptionWhenDepositValueIsZero() {
        Integer customerId = 1;
        Integer accountId = 2;

        Exception exception = assertThrows(BusinessException.class, () -> accountService.deposit(customerId, accountId, 0.0));
        assertEquals("Deposit value must be greater than 0", exception.getMessage());
    }

    @Test
    public void shouldIncreaseAccountBalanceWhenDepositValueIsGreaterThanZero() throws BusinessException {
        Integer customerId = 1;
        Integer accountId = 2;

        Account account = new Account();
        account.setBalance(300.0);

        given(accountRepositoryMock.findByCustomerIdAndId(eq(customerId), eq(accountId))).willReturn(Optional.of(account));
        given(accountRepositoryMock.save(eq(account))).willReturn(account);

        account = accountService.deposit(customerId, accountId, 800.0);

        assertEquals(1100, account.getBalance());
    }

    @Test
    public void shouldThrowExceptionWhenSourceAccountNotFound() {
        Integer customerId = 1;
        Integer sourceAccountId = 2;
        Integer targetAccountId = 3;

        given(accountRepositoryMock.findByCustomerIdAndId(eq(customerId), eq(sourceAccountId))).willReturn(Optional.empty());

        Exception exception = assertThrows(BusinessException.class, () -> accountService.transfer(customerId, sourceAccountId, targetAccountId, 1000.0));
        assertEquals(String.format("Account with id %d not found", sourceAccountId), exception.getMessage());
    }

    @Test
    public void shouldThrowExceptionWhenTargetAccountNotFound() {
        Integer customerId = 1;
        Integer sourceAccountId = 2;
        Integer targetAccountId = 3;

        given(accountRepositoryMock.findByCustomerIdAndId(eq(customerId), eq(sourceAccountId))).willReturn(Optional.of(new Account()));
        given(accountRepositoryMock.findById(eq(targetAccountId))).willReturn(Optional.empty());

        Exception exception = assertThrows(BusinessException.class, () -> accountService.transfer(customerId, sourceAccountId, targetAccountId, 1000.0));
        assertEquals(String.format("Account with id %d not found", targetAccountId), exception.getMessage());
    }

    @Test
    public void shouldThrowExceptionWhenSourceAccountDoesNotHaveEnoughCash() {
        Integer customerId = 1;
        Integer sourceAccountId = 2;
        Integer targetAccountId = 3;
        Account account = new Account();
        account.setBalance(100.0);

        given(accountRepositoryMock.findByCustomerIdAndId(eq(customerId), eq(sourceAccountId))).willReturn(Optional.of(account));
        given(accountRepositoryMock.findById(eq(targetAccountId))).willReturn(Optional.of(new Account()));

        Exception exception = assertThrows(BusinessException.class, () -> accountService.transfer(customerId, sourceAccountId, targetAccountId, 1000.0));
        assertEquals(String.format("Not enough cash to transfer", targetAccountId), exception.getMessage());
    }

    @Test
    public void ShouldTransferFromOneAccountToAnother() throws BusinessException {
        Integer customerId = 1;
        Integer sourceAccountId = 2;
        Integer targetAccountId = 3;

        Account sourceAccount = new Account();
        sourceAccount.setBalance(800.0);

        Account targetAccount = new Account();
        targetAccount.setBalance(400.0);

        given(accountRepositoryMock.findByCustomerIdAndId(eq(customerId), eq(sourceAccountId))).willReturn(Optional.of(sourceAccount));
        given(accountRepositoryMock.findById(eq(targetAccountId))).willReturn(Optional.of(targetAccount));

        accountService.transfer(customerId, sourceAccountId, targetAccountId, 500.0);

        assertEquals(300.0, sourceAccount.getBalance(), 0.01);
        assertEquals(900.0, targetAccount.getBalance(), 0.01);
    }
}