package b10.yu952345.foundation.bank.yu952345_bank.account;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = AccountController.class)
class AccountControllerTest {

    private static final String BASE_URL = "/api/customers/%d/accounts";
    private static final String CONTENT_TYPE = "application/json";

    @MockBean
    private AccountService accountServiceMock;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void shouldCreateAccountForUser() throws Exception {
        Integer customerId = 1;

        Account account = new Account();
        account.setType(AccountType.SAVINGS);

        Account expectedAccount = new Account();
        expectedAccount.setId(2);
        expectedAccount.setType(account.getType());
        expectedAccount.setBalance(0.0);

        given(accountServiceMock.create(eq(customerId), any(Account.class)))
                .willReturn(expectedAccount);

        mockMvc.perform(
                post(String.format(BASE_URL, customerId))
                        .content((objectMapper.writeValueAsString(account)))
                        .contentType(CONTENT_TYPE))
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.id", is(expectedAccount.getId())))
        .andExpect(jsonPath("$.type", is(expectedAccount.getType().toString())))
        .andExpect(jsonPath("$.balance", is(expectedAccount.getBalance())));
    }

    @Test
    public void shouldGetAccountForUser() throws Exception {
        Integer customerId = 1;
        Integer accountId = 2;

        Account expectedAccount = new Account();
        expectedAccount.setId(accountId);
        expectedAccount.setType(AccountType.CURRENT);
        expectedAccount.setBalance(12.21);

        given(accountServiceMock.findById(eq(customerId), eq(accountId)))
                .willReturn(expectedAccount);

        mockMvc.perform(
                get(String.format(BASE_URL + "/%d", customerId, accountId)))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id", is(expectedAccount.getId())))
        .andExpect(jsonPath("$.type", is(expectedAccount.getType().toString())))
        .andExpect(jsonPath("$.balance", is(expectedAccount.getBalance())));
    }

    @Test
    public void shouldDepositValueIntoAccountForUser() throws Exception {
        Integer customerId = 1;
        Integer accountId = 2;
        Double depositValue = 123.45;

        Account expectedAccount = new Account();
        expectedAccount.setId(accountId);
        expectedAccount.setType(AccountType.CURRENT);
        expectedAccount.setBalance(depositValue);

        given(accountServiceMock.deposit(eq(customerId), eq(accountId), anyDouble()))
                .willReturn(expectedAccount);

        mockMvc.perform(
                put(String.format(BASE_URL + "/%d", customerId, accountId))
                        .content(objectMapper.writeValueAsString(depositValue))
                        .contentType(CONTENT_TYPE))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id", is(expectedAccount.getId())))
        .andExpect(jsonPath("$.type", is(expectedAccount.getType().toString())))
        .andExpect(jsonPath("$.balance", is(expectedAccount.getBalance())));
    }

    @Test
    public void shouldTransferValueFromAccountForUser() throws Exception {
        Integer customerId = 1;
        Integer sourceAccountId = 2;
        Integer targetAccountId = 3;
        Double transferValue = 123.45;

        mockMvc.perform(
                put(String.format(BASE_URL + "/%d/%d", customerId, sourceAccountId, targetAccountId))
                        .content(objectMapper.writeValueAsString(transferValue))
                        .contentType(CONTENT_TYPE))
        .andExpect(status().isOk());
    }
}