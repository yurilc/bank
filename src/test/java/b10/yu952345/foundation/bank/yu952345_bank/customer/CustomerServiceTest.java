package b10.yu952345.foundation.bank.yu952345_bank.customer;

import b10.yu952345.foundation.bank.yu952345_bank.account.AccountType;
import b10.yu952345.foundation.bank.yu952345_bank.exception.BusinessException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class CustomerServiceTest {

    @InjectMocks
    private CustomerService customerService;

    @Mock
    private CustomerRepository customerRepository;

    @Test
    public void shouldCreateCustomerWithAccount() {
        Customer customer = new Customer();
        customer.setId(1);

        customerService.createCustomer(customer);

        verify(customerRepository, times(1)).save(eq(customer));

        assertNull(customer.getId());
        assertNotNull(customer.getAccounts());
        assertEquals(AccountType.CURRENT, customer.getAccounts().get(0).getType());
        assertEquals(0.0, customer.getAccounts().get(0).getBalance(), 0.01);
    }

    @Test
    public void shouldThrowExceptionWhenUpdatingAnMissingCustomer() {
        Integer customerId = 1;
        Customer customer = new Customer();

        given(customerRepository.findById(eq(customerId))).willReturn(Optional.empty());

        Exception exception = assertThrows(BusinessException.class, () -> customerService.update(customerId, customer));
        assertEquals(String.format("Customer with id %d not found", customerId), exception.getMessage());
    }

    @Test
    public void shouldUpdateCustomer() throws BusinessException {
        Integer customerId = 1;
        Customer customer = new Customer();
        customer.setFirstName("some name");
        customer.setLastName("some last name");
        customer.setPassword("some password");

        Customer expectedCustomer = new Customer();

        given(customerRepository.findById(eq(customerId))).willReturn(Optional.of(expectedCustomer));
        given(customerRepository.save(eq(expectedCustomer))).willReturn(expectedCustomer);

        Customer updated = customerService.update(customerId, customer);

        assertEquals(updated.getFirstName(), customer.getFirstName());
        assertEquals(updated.getLastName(), customer.getLastName());
        assertEquals(updated.getPassword(), customer.getPassword());
    }

    @Test
    public void shouldFindAllCustomers() {
        customerService.findAll();

        verify(customerRepository, times(1)).findAll();
    }

    @Test
    public void shouldThrowExceptionWhenCustomerNotFound() {
        Integer customerId = 1;

        given(customerRepository.findById(eq(customerId))).willReturn(Optional.empty());

        Exception exception = assertThrows(BusinessException.class, () -> customerService.findById(customerId));

        assertEquals(String.format("Customer with id %d not found", customerId), exception.getMessage());
    }

    @Test
    public void shouldReturnCustomerWhenFindById() throws BusinessException {
        Integer customerId = 1;

        given(customerRepository.findById(eq(customerId))).willReturn(Optional.of(new Customer()));

        Customer customer = customerService.findById(customerId);

        assertNotNull(customer);
    }

    @Test
    public void shouldThrowExceptionWhenDeleteByIdNotFound() {
        Integer customerId = 1;

        Exception exception = assertThrows(BusinessException.class, () -> customerService.delete(customerId));

        assertEquals(String.format("Customer with id %d not found", customerId), exception.getMessage());
    }

    @Test
    public void shouldDeleteById() throws BusinessException {
        Integer customerId = 1;
        Customer customer = new Customer();

        given(customerRepository.findById(eq(customerId))).willReturn(Optional.of(customer));

        customerService.delete(customerId);

        verify(customerRepository, times(1)).delete(eq(customer));
    }
}