package b10.yu952345.foundation.bank.yu952345_bank.customer;

import b10.yu952345.foundation.bank.yu952345_bank.account.Account;
import b10.yu952345.foundation.bank.yu952345_bank.exception.BusinessException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = CustomerController.class)
class CustomerControllerTest {

    public static final String BASE_URL = "/api/customers";
    public static final String CONTENT_TYPE = "application/json";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private CustomerService customerServiceMock;

    @Test
    public void shouldCreateCustomer() throws Exception {
        Customer expectedCustomer = new Customer();
        expectedCustomer.setId(1);
        expectedCustomer.setFirstName("some name");
        expectedCustomer.setLastName("some last name");
        expectedCustomer.setPassword("some password");
        expectedCustomer.addAccount(new Account());

        given(customerServiceMock.createCustomer(any(Customer.class))).willReturn(expectedCustomer);

        mockMvc.perform(
                post(BASE_URL)
                .contentType(CONTENT_TYPE)
                .content(objectMapper.writeValueAsString(new Customer())))
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.id", is(expectedCustomer.getId())))
        .andExpect(jsonPath("$.firstName", is(expectedCustomer.getFirstName())))
        .andExpect(jsonPath("$.lastName", is(expectedCustomer.getLastName())))
        .andExpect(jsonPath("$.password").doesNotExist())
        .andExpect(jsonPath("$.accounts[0].type").hasJsonPath());
    }

    @Test
    public void shouldThrowExceptionWhenCustomerNotFound() throws Exception {
        Integer customerId = 1;
        String expectedErrorMessage = "customer not fount";

        when(customerServiceMock.update(eq(customerId), any(Customer.class))).thenThrow(new BusinessException(expectedErrorMessage));

        mockMvc.perform(
                put(BASE_URL + "/" + customerId)
                        .contentType(CONTENT_TYPE)
                        .content(objectMapper.writeValueAsString(new Customer())))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.message", is("Business Exception")))
        .andExpect(jsonPath("$.details", is(expectedErrorMessage)));
    }

    @Test
    public void shouldUpdateCustomer() throws Exception {
        Integer customerId = 1;
        Customer expectedCustomer = new Customer();
        expectedCustomer.setId(customerId);
        expectedCustomer.setFirstName("some name");
        expectedCustomer.setLastName("some last name");
        expectedCustomer.setPassword("some password");
        expectedCustomer.addAccount(new Account());

        given(customerServiceMock.update(eq(customerId), any(Customer.class))).willReturn(expectedCustomer);

        mockMvc.perform(
                put(BASE_URL + "/" + customerId)
                        .contentType(CONTENT_TYPE)
                        .content(objectMapper.writeValueAsString(new Customer())))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id", is(expectedCustomer.getId())))
        .andExpect(jsonPath("$.firstName", is(expectedCustomer.getFirstName())))
        .andExpect(jsonPath("$.lastName", is(expectedCustomer.getLastName())))
        .andExpect(jsonPath("$.password").doesNotExist())
        .andExpect(jsonPath("$.accounts[0].type").hasJsonPath());
    }

    @Test
    public void shouldFindAllCustomers() throws Exception {
        List<Customer> customers = new ArrayList<>();
        Customer customer = new Customer();
        customer.setId(1);
        customer.setFirstName("some name");
        customer.setLastName("some last name");
        customer.setPassword("some password");
        customer.addAccount(new Account());
        customers.add(customer);

        given(customerServiceMock.findAll()).willReturn(customers);

        mockMvc.perform(
                get(BASE_URL))
                .andExpect(status().isOk())
        .andExpect(jsonPath("$[0].id", is(customer.getId())))
        .andExpect(jsonPath("$[0].firstName", is(customer.getFirstName())))
        .andExpect(jsonPath("$[0].lastName", is(customer.getLastName())))
        .andExpect(jsonPath("$[0].password").doesNotExist())
        .andExpect(jsonPath("$[0].accounts[0].type").hasJsonPath());
    }

    @Test
    public void shouldFindById() throws Exception {
        Integer customerId = 1;
        Customer customer = new Customer();
        customer.setId(customerId);
        customer.setFirstName("some name");
        customer.setLastName("some last name");
        customer.setPassword("some password");
        customer.addAccount(new Account());

        given(customerServiceMock.findById(eq(customerId))).willReturn(customer);

        mockMvc.perform(
                get(BASE_URL + "/" + customerId))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id", is(customer.getId())))
        .andExpect(jsonPath("$.firstName", is(customer.getFirstName())))
        .andExpect(jsonPath("$.lastName", is(customer.getLastName())))
        .andExpect(jsonPath("$.password").doesNotExist())
        .andExpect(jsonPath("$.accounts[0].type").hasJsonPath());
    }

    @Test
    public void shouldDeleteById() throws Exception {
        mockMvc.perform(
                delete(BASE_URL + "/" + 1))
        .andExpect(status().isNoContent());
    }

}