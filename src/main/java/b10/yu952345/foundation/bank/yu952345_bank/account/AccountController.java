package b10.yu952345.foundation.bank.yu952345_bank.account;

import b10.yu952345.foundation.bank.yu952345_bank.exception.BusinessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/customers/{customerId}/accounts")
public class AccountController {

    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping
    public ResponseEntity<Account> create(@PathVariable("customerId") Integer customerId, @RequestBody Account account) throws BusinessException {
        return ResponseEntity.status(HttpStatus.CREATED).body(accountService.create(customerId, account));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Account> getById(@PathVariable("customerId") Integer customerId, @PathVariable("id") Integer accountId) {
        return ResponseEntity.ok(accountService.findById(customerId, accountId));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Account> deposit(@PathVariable("customerId") Integer customerId, @PathVariable("id") Integer accountId, @RequestBody Double value) throws BusinessException {
        return ResponseEntity.ok(accountService.deposit(customerId, accountId, value));
    }

    @PutMapping("/{sourceId}/{targetId}")
    public ResponseEntity transfer(@PathVariable("customerId") Integer customerId, @PathVariable("sourceId") Integer sourceId, @PathVariable("targetId") Integer targetId, @RequestBody Double value) throws BusinessException {
        accountService.transfer(customerId, sourceId, targetId, value);
        return ResponseEntity.ok().build();
    }
}
