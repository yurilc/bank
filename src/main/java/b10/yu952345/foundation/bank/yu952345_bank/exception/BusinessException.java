package b10.yu952345.foundation.bank.yu952345_bank.exception;

public class BusinessException extends Exception {
    public BusinessException(String message) {
        super(message);
    }
}
