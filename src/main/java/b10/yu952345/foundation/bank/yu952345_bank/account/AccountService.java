package b10.yu952345.foundation.bank.yu952345_bank.account;

import b10.yu952345.foundation.bank.yu952345_bank.customer.Customer;
import b10.yu952345.foundation.bank.yu952345_bank.customer.CustomerRepository;
import b10.yu952345.foundation.bank.yu952345_bank.exception.BusinessException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AccountService {

    private static final String ACCOUNT_WITH_ID_D_NOT_FOUND = "Account with id %d not found";

    private final AccountRepository accountRepository;

    private final CustomerRepository customerRepository;

    public AccountService(AccountRepository accountRepository, CustomerRepository customerRepository) {
        this.accountRepository = accountRepository;
        this.customerRepository = customerRepository;
    }

    public Account create(Integer customerId, Account account) throws BusinessException {
        Optional<Customer> customerOptional = customerRepository.findById(customerId);
        if(customerOptional.isEmpty()) {
            throw new BusinessException(String.format("Customer with id %d not found", customerId));
        }
        account.setCustomerId(customerId);
        account.setBalance(0.0);

        Customer customer = customerOptional.get();
        customer.addAccount(account);
        account.setCustomer(customer);

        account = accountRepository.save(account);
        customerRepository.save(customer);

        return account;
    }

    public Account findById(Integer customerId, Integer id) {
        return accountRepository.findByCustomerIdAndId(customerId, id).orElse(null);
    }

    public Account deposit(Integer customerId, Integer id, Double value) throws BusinessException {
        if(value < 1) {
            throw new BusinessException("Deposit value must be greater than 0");
        }

        Optional<Account> accountOptional = accountRepository.findByCustomerIdAndId(customerId, id);
        if (accountOptional.isEmpty()) {
            throw new BusinessException(String.format("Account not found for customer id %d and account id %d", customerId, id));
        }
        Account account = accountOptional.get();

        account.setBalance(account.getBalance() + value);

        return accountRepository.save(account);
    }

    public void transfer(Integer customerId, Integer sourceId, Integer targetId, Double value) throws BusinessException {
        Optional<Account> sourceAccountOptional = accountRepository.findByCustomerIdAndId(customerId, sourceId);
        if(sourceAccountOptional.isEmpty()) {
            throw new BusinessException(String.format(ACCOUNT_WITH_ID_D_NOT_FOUND, sourceId));
        }

        Optional<Account> targetAccountOptional = accountRepository.findById(targetId);
        if(targetAccountOptional.isEmpty()) {
            throw new BusinessException(String.format(ACCOUNT_WITH_ID_D_NOT_FOUND, targetId));
        }

        Account sourceAccount = sourceAccountOptional.get();
        Account targetAccount = targetAccountOptional.get();

        if(sourceAccount.getBalance() - value < 0) {
            throw new BusinessException("Not enough cash to transfer");
        }

        sourceAccount.setBalance(sourceAccount.getBalance() - value);
        targetAccount.setBalance(targetAccount.getBalance() + value);

        accountRepository.save(sourceAccount);
        accountRepository.save(targetAccount);
    }
}
