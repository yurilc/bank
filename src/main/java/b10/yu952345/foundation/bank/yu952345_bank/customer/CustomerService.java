package b10.yu952345.foundation.bank.yu952345_bank.customer;

import b10.yu952345.foundation.bank.yu952345_bank.account.Account;
import b10.yu952345.foundation.bank.yu952345_bank.account.AccountType;
import b10.yu952345.foundation.bank.yu952345_bank.exception.BusinessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {
    public static final String CUSTOMER_WITH_ID_D_NOT_FOUND = "Customer with id %d not found";
    private final CustomerRepository customerRepository;

    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public Customer createCustomer(Customer customer) {
        customer.setId(null);

        Account account = new Account();
        account.setType(AccountType.CURRENT);
        account.setBalance(0.0);
        account.setCustomer(customer);

        customer.addAccount(account);

        return customerRepository.save(customer);
    }

    public Customer update(Integer id, Customer customer) throws BusinessException {
        Optional<Customer> customerOptional = customerRepository.findById(id);
        if(customerOptional.isEmpty()) {
            throw new BusinessException(String.format(CUSTOMER_WITH_ID_D_NOT_FOUND, id));
        }
        Customer existing = customerOptional.get();
        existing.setFirstName(customer.getFirstName());
        existing.setLastName(customer.getLastName());
        existing.setPassword(customer.getPassword());

        return customerRepository.save(existing);
    }

    public List<Customer> findAll() {
        return customerRepository.findAll();
    }

    public Customer findById(Integer id) throws BusinessException {
        Optional<Customer> customerOptional = customerRepository.findById(id);
        if(customerOptional.isEmpty()) {
            throw new BusinessException(String.format(CUSTOMER_WITH_ID_D_NOT_FOUND, id));
        }
        return customerOptional.get();
    }

    public void delete(Integer id) throws BusinessException {
        Optional<Customer> customerOptional = customerRepository.findById(id);
        if(customerOptional.isEmpty()) {
            throw new BusinessException(String.format(CUSTOMER_WITH_ID_D_NOT_FOUND, id));
        }
        customerRepository.delete(customerOptional.get());
    }
}
