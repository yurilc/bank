package b10.yu952345.foundation.bank.yu952345_bank.util;

import b10.yu952345.foundation.bank.yu952345_bank.exception.BusinessException;

import java.util.Objects;

public class ErrorResponse {
    private String message;
    private String details;

    public ErrorResponse(BusinessException exception) {
        this.message = "Business Exception";
        this.details = exception.getMessage();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ErrorResponse that = (ErrorResponse) o;
        return Objects.equals(message, that.message) &&
                Objects.equals(details, that.details);
    }

    @Override
    public int hashCode() {
        return Objects.hash(message, details);
    }

    @Override
    public String toString() {
        return "ErrorResponse{" +
                "message='" + message + '\'' +
                ", details='" + details + '\'' +
                '}';
    }
}
