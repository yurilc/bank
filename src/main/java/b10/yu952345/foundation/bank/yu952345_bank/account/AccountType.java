package b10.yu952345.foundation.bank.yu952345_bank.account;

public enum AccountType {
    CURRENT, SAVINGS
}
