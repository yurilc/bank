package b10.yu952345.foundation.bank.yu952345_bank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Yu952345BankApplication {

	public static void main(String[] args) {
		SpringApplication.run(Yu952345BankApplication.class, args);
	}

}
